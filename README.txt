Homework 1 readme
=================

##How to build and run##
The pom.xml file is configured to build the solution for Hadoop 2.6.0, simply run
mvn package

The jar file then is createn in the target/ folder so just run the jar using
hadoop jar target/homework1-1.0.jar Homework1 <input dir> <output dir>


##Links and ideas behind the homework 1##

The API was of course of great help: http://hadoop.apache.org/docs/r2.6.0/api/

After a short websearch it apeared that the following link would be of great help
http://cotdp.com/2012/07/hadoop-processing-zip-files-in-mapreduce/
an additional useful resource was http://stackoverflow.com/questions/14497572/reading-gzipped-file-in-hadoop-using-custom-recordreader (which just confirmed I was on the right track)

The main pipeline idea is to use a custom FileInputFormat to decompress the files, use the defaul identity mapper to pass the data to the reducers (which makes all the nodes work on files) and then the reducer writes the received bytes to a HDFS file created in the output directory. NullOutputFormat is used to avoid having any SUCCESS or part-00000 files.
Since the whole pipeline uses bytes there is no difference between textfiles or any other types. For instance it works perfectly with an audio file.

This design however has the flaw of not being well loadbalanced, since I did not refine how the tasks are distributed among reducer nodes. This implies that if we have two reducer nodes and three files, say two of 1MB and one of 1GB, then there is no control whether the files will be splitted correctly (i.e. one reducer gets two time 1MB and the other gets 1GB).

Something that I did not have time to look at is the container count restriction, but I am quite sure that it is only a matter of knowing well the API and adding a few lines of code to Homework1.java.

Even though this homework was "not a hadr programming task" as Prof. Koch said, it was tedious to dive into the whole Hadoop API and it took a lot of time. This is why my jar still has some flaws and seems not to complete the 'big/' dataset we were provided. However on smaller inputs it produces identical files (md5 checksum is correct). I estimate the overall time spent to be around 12 hours.

One last word about academic honesty: I hereby certify that I did not share my solution with any other participant nor received any code nor links from them.

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.EOFException;
import java.util.zip.GZIPInputStream;
//import java.lang.Long;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
//import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
//import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
//import org.apache.hadoop.mapreduce.Job;
//import org.apache.hadoop.mapreduce.Mapper;
//import org.apache.hadoop.mapreduce.Reducer;
//import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
//import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
//import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class GzFileRecordReader
	extends RecordReader<Text, BytesWritable> {

	private FSDataInputStream fsin;
	private GZIPInputStream gzis;
	private Text currKey;
	private BytesWritable currValue;
	private boolean isFinished = false;

	@Override
	public void initialize(InputSplit inputSplit, TaskAttemptContext taContext)
	    throws IOException, InterruptedException {

	    FileSplit split = (FileSplit) inputSplit;
	    Configuration conf = taContext.getConfiguration();
	    Path path = split.getPath();
	    currKey = new Text(path.getName());
	    FileSystem fs = path.getFileSystem(conf);

	    fsin = fs.open(path);
	    gzis = new GZIPInputStream(fsin);
	}

	@Override
	public boolean nextKeyValue()
	    throws IOException, InterruptedException {

	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    byte[] buffer = new byte[2*4096];

	    int bytesRead = 0;
	    bytesRead = gzis.read(buffer, 0, 2*4096);
	    if (bytesRead > 0) {
		bos.write(buffer, 0, bytesRead);
	    } else {
		isFinished = true;
		return false;
	    }
	    currValue = new BytesWritable(bos.toByteArray());

	    return true;
	}

	@Override
	public float getProgress()
	    throws IOException, InterruptedException {

	    return isFinished ? 1 : 0;
	}


	@Override
	public Text getCurrentKey()
	    throws IOException, InterruptedException {

	    return currKey;
	}

	@Override
	public BytesWritable getCurrentValue()
	    throws IOException, InterruptedException {

	    return currValue;
	}

	@Override
	public void close()
	    throws IOException {

	    try { gzis.close(); } catch ( Exception ignore ) { }
	    try { fsin.close(); } catch ( Exception ignore ) { }
	}
    }

//import java.io.ByteArrayOutputStream;
import java.io.IOException;
//import java.io.EOFException;
//import java.util.zip.GZIPInputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
//import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
//import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
//import org.apache.hadoop.mapreduce.InputSplit;
//import org.apache.hadoop.mapreduce.JobContext;
//import org.apache.hadoop.mapreduce.RecordReader;
//import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
//import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
//import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;

public class Homework1 {

    public static class GzReducer
	extends Reducer<Text,BytesWritable,Object,Object> {

	public void reduce(Text key, Iterable<BytesWritable> values,
			   Context context
			   ) throws IOException, InterruptedException {

	    // creating the output stream to write the file
	    FileSystem fs = FileOutputFormat.getOutputPath(context).getFileSystem(context.getConfiguration());
	    String filename = key.toString();
	    
	    Path file = new Path(FileOutputFormat.getOutputPath(context),
				 filename.substring(0,filename.length()-3));
	    System.out.println("Writing to file "+file.toString());
	    if (fs.exists(file))
		fs.delete(file,true);
	    FSDataOutputStream out = fs.create(file);

	    for (BytesWritable bw: values) {
		//String value = bw.toString();
		//System.out.println("Key = " + key.toString().substring(0,key.toString().length()-3) + ", value = " + value + ", offset = " + ib.getOffset());

		out.write(bw.getBytes(),0,bw.getLength());
	    }

	    Path tempFile = new Path(FileOutputFormat.getOutputPath(context),
				 "_temporary");
	    System.out.println("Deleting temporary file");
	    if (fs.exists(tempFile))
		fs.delete(tempFile,true);
	    out.close();
	}
    }

    public static void main(String[] args) throws Exception {
	Configuration conf = new Configuration();
	Job job = Job.getInstance(conf, "homework 1");
	job.setJarByClass(Homework1.class);
	job.setReducerClass(GzReducer.class);

	job.setInputFormatClass(GzFileInputFormat.class);
	job.setMapOutputKeyClass(Text.class);
	job.setMapOutputValueClass(BytesWritable.class);
	job.setOutputFormatClass(NullOutputFormat.class);

	GzFileInputFormat.setInputPaths(job, new Path(args[0]+"/*.gz"));
	FileOutputFormat.setOutputPath(job, new Path(args[1]));

	System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}

